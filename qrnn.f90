module mod_qrnn

    use iso_fortran_env, only: DP => REAL64
    use mod_atomic_property, only: SIGMA2

    implicit none

    private 
    public :: calc_R, calc_J, charge_recursion

contains

    pure subroutine calc_R( &
        nat, &
        xyz, &
        R &
    )
        integer, intent(in) :: nat
        real(DP), intent(in) :: xyz(3, nat)
        real(DP), intent(out)  :: R(nat, nat)
        integer :: i, j
        do i = 1, nat
            do j = 1, nat
                R(i, j) = norm2(xyz(:, i) - xyz(:, j))
            end do
        end do
    end subroutine

    pure subroutine calc_J( &
        nat, &
        atomic_number, &
        R, &
        Jmat &
    )
        integer, intent(in) :: nat
        integer, intent(in) :: atomic_number(nat)
        real(DP), intent(in) :: R(nat, nat)
        real(DP), intent(out) :: Jmat(nat, nat)

        integer :: i, j

        ! Compute J
        do i = 1, nat
            do j = 1, nat
                if (i == j) cycle
                Jmat(i, j) = &
                    erf( &
                                                           R(i, j) / &
                !       ---------------------------------------------------------------------
                        sqrt(2._DP * (SIGMA2(atomic_number(i)) + SIGMA2(atomic_number(j)))) &
                    ) / &
                !   ------------------------------------------------------------------------------
                                                           R(i, j)
            end do
        end do
    end subroutine

    recursive subroutine charge_recursion( &
            nat, &
            Jmat, &
            inverse_hardness, &
            chi, &
            Q_tot, &
            charge &
        )
        integer, intent(in) :: nat
        real(DP), intent(in) :: Jmat(nat, nat)
        real(DP), intent(in) :: inverse_hardness(nat)
        real(DP), intent(in) :: chi(nat)
        real(DP), intent(in) :: Q_tot
        real(DP), intent(in out) :: charge(nat)

        real(DP), parameter :: PRECISION_CRITERIA = 2E-2_DP
        real(DP), dimension(nat) :: effective_chi, lambda, old_charge
        integer :: i, j

        old_charge = charge

        ! Compute effective_chi
        effective_chi = chi
        do i = 1, nat
            do j = 1, nat
                if (i == j) cycle
                effective_chi(i) = effective_chi(i) + 0.5_DP * Jmat(i, j) * charge(j)
            end do
        end do

        ! Compute charges
        charge = - inverse_hardness * (effective_chi - ( &
        !        Lambda
                 (Q_tot + sum(effective_chi * inverse_hardness)) / &
        !        -----------------------------------------------------
                             sum(inverse_hardness) &
        ))

        if (sum((charge - old_charge)**2) > precision_criteria) &
            call charge_recursion(nat, Jmat, inverse_hardness, chi, Q_tot, charge)

    end subroutine charge_recursion

end module mod_qrnn


