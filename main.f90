program test

    use iso_fortran_env, only: DP => REAL64
    use mod_qrnn, only: calc_R, calc_J, charge_recursion
    use mod_atomic_property, only: get_inverse_hardness, get_chi

    implicit none

    integer, parameter :: nat = 20
    real(DP), dimension(nat) :: charge, inverse_hardness, chi
    real(DP), dimension(nat, nat) :: J, R
    real(DP), dimension(3, nat) :: xyz
    integer, dimension(nat) :: atomic_number
    real(DP) :: Q_tot

    Q_tot = sum(charge)
    call calc_R(nat, xyz, R)
    call calc_J(nat, atomic_number, R, J)
    inverse_hardness = get_inverse_hardness(atomic_number)
    chi = get_chi(atomic_number)
    ! intial charge
    charge = 0.0_DP
    call charge_recursion(nat, J, inverse_hardness, chi, Q_tot, charge)
end program
