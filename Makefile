main: main.o qrnn.o atomic_property.o
	$(FC) $(LDFLAGS) $^ -o $@
qrnn.o: atomic_property.o
%.o: %.f90
	$(FC) $(FFLAGS) -c $< -o $@

