module mod_atomic_property

    use iso_fortran_env, only: DP => REAL64

    implicit none

    private
    public :: get_inverse_hardness, get_chi, sigma2

    real(DP), parameter :: pi = 3.141592653589793_DP
    ! covalet radius in Angstrom taken from WebElements: http://www.webelements.com/periodicity/covalent_radius/
    real(DP), parameter :: sigma(118) = [ &
        0.37_DP, 0.32_DP, 1.34_DP, 0.90_DP, 0.82_DP, 0.77_DP, 0.75_DP, 0.73_DP, 0.71_DP, 0.69_DP, 1.54_DP, &
        1.30_DP, 1.18_DP, 1.11_DP, 1.06_DP, 1.02_DP, 0.99_DP, 0.97_DP, 1.96_DP, 1.74_DP, 1.44_DP, 1.36_DP, &
        1.25_DP, 1.27_DP, 1.39_DP, 1.25_DP, 1.26_DP, 1.21_DP, 1.38_DP, 1.31_DP, 1.26_DP, 1.22_DP, 1.19_DP, &
        1.16_DP, 1.14_DP, 1.10_DP, 2.11_DP, 1.92_DP, 1.62_DP, 1.48_DP, 1.37_DP, 1.45_DP, 1.56_DP, 1.26_DP, &
        1.35_DP, 1.31_DP, 1.53_DP, 1.48_DP, 1.44_DP, 1.41_DP, 1.38_DP, 1.35_DP, 1.33_DP, 1.30_DP, 2.25_DP, &
        1.98_DP, 1.69_DP, -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , &
        -1._DP , -1._DP , -1._DP , -1._DP , 1.60_DP, 1.50_DP, 1.38_DP, 1.46_DP, 1.59_DP, 1.28_DP, 1.37_DP, &
        1.28_DP, 1.44_DP, 1.49_DP, 1.48_DP, 1.47_DP, 1.46_DP, -1._DP , -1._DP , 1.45_DP, -1._DP , -1._DP , &
        -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , &
        -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , &
        -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP , -1._DP &
    ]
    real(DP), parameter :: inverse_hardness(118) = sqrt(pi) * sigma
    ! real(DP), parameter :: chi(118) = []
    real(DP), parameter :: chi(118) = 0.0_DP
    real(DP), parameter :: sigma2(118) = sigma ** 2

contains

    real(DP) elemental function get_inverse_hardness(i)
        integer, intent(in) :: i
        get_inverse_hardness = inverse_hardness(i)
    end function

    real(DP) elemental function get_chi(i)
        integer, intent(in) :: i
        get_chi = chi(i)
    end function

end module mod_atomic_property
